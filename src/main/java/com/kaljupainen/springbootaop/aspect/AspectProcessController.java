package com.kaljupainen.springbootaop.aspect;

import com.kaljupainen.springbootaop.exceptions.ProcessException;
import java.lang.reflect.Method;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
public class AspectProcessController {

  @Before("execution(* com.kaljupainen.springbootaop.service.*.*(..))")
  public void before(JoinPoint joinPoint) {
    if (joinPoint != null) {
      System.out.println("Executing before " + joinPoint);
    }
  }

  @AfterThrowing(
      value = "execution(* com.kaljupainen.springbootaop.service.*.*(..))",
      throwing = "ex")
  public void whenMethodFails(JoinPoint joinPoint, ProcessException ex) {
    try {
      System.out.println(
          "Exception raised, so can't continue process " + joinPoint.getTarget().toString());
      MethodSignature signature = (MethodSignature) joinPoint.getSignature();
      Method method = signature.getMethod();
      System.out.println("Method failed is " + method.getName() + " in process " + ex);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
