package com.kaljupainen.springbootaop.exceptions;

public class ProcessException extends Exception {

  /** */
  private static final long serialVersionUID = 1L;

  public ProcessException(String idOfProcess) {
    super("id of process: " + idOfProcess);
  }
}
