package com.kaljupainen.springbootaop.controller;

import com.kaljupainen.springbootaop.exceptions.ProcessException;
import com.kaljupainen.springbootaop.service.ProcessInterface;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProcessExecution {
  @Autowired ProcessInterface process;

  public void execute() throws ProcessException {
    String idProcess = UUID.randomUUID().toString();
    process.setId(idProcess);
    process.step1();
    process.step2();
  }
}
