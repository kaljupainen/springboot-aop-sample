package com.kaljupainen.springbootaop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kaljupainen.springbootaop.exceptions.ProcessException;
import com.kaljupainen.springbootaop.service.ProcessInterface;

@RestController
public class Controller {
	
	@Autowired
	ProcessExecution execution;
	
	@GetMapping("/greeting")
	public String greeting() {
		try {
			for (int i=0;i<10;i++) {
				execution.execute();
			} 
		} catch (ProcessException e) {
			System.out.println("Exception -> returning custom message");
			return "exception";
		}		
		return "ok";
	}
}
