package com.kaljupainen.springbootaop.service;

import com.kaljupainen.springbootaop.exceptions.ProcessException;

public interface ProcessInterface {
  public void setId(String id);

  public void step1() throws ProcessException;

  public void step2() throws ProcessException;
}
