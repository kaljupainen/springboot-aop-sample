package com.kaljupainen.springbootaop.service.impl;

import com.kaljupainen.springbootaop.exceptions.ProcessException;
import com.kaljupainen.springbootaop.service.ProcessInterface;
import java.util.Random;
import org.springframework.stereotype.Service;

@Service
public class ProcessImpl implements ProcessInterface {

  private String idOfProcess;

  public void setId(String id) {
    idOfProcess = id;
  }

  @Override
  public void step1() throws ProcessException {
    System.out.println("Step 1");
    Random rd = new Random();
    int n = rd.nextInt(10);
    System.out.println("Number " + n);
    if (n > 5) {
      throw new ProcessException(idOfProcess);
    }
  }

  @Override
  public void step2() throws ProcessException {
    System.out.println("Step 2");
    Random rd = new Random();
    int n = rd.nextInt(10);
    System.out.println("Number " + n);
    if (n > 5) {
      throw new ProcessException(idOfProcess);
    }
  }
}
